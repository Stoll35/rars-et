/****************************************************************************
** Form interface generated from reading ui file './kde_initcar.ui'
**
** Created: Mi Jul 24 14:56:09 2013
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KDEINITCAR_H
#define KDEINITCAR_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QGroupBox;
class QListBox;
class QListBoxItem;

class KdeInitCar : public QDialog
{
    Q_OBJECT

public:
    KdeInitCar( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~KdeInitCar();

    QLabel* TextLabel1;
    QGroupBox* GroupBox1;
    QListBox* ListMessage;

protected:

protected slots:
    virtual void languageChange();

};

#endif // KDEINITCAR_H
